<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
	<div class="container-fluid">
	  <div class="pull-right">
		<button type="submit" form="form-agv-saga" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-check-circle"></i></button>
		<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
	  <h1><?php echo $heading_title; ?></h1>
	  <ul class="breadcrumb">
		<?php foreach ($breadcrumbs as $breadcrumb) { ?>
			<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
		<?php } ?>
	  </ul>
	</div>
  </div>
  <div class="container-fluid">
	<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
		  <button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
	<?php } ?>
	<div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-" class="form-horizontal">
		  <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="agv_saga_module_status" id="input-status" class="form-control">
				<?php if ($agv_saga_module_status) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
				<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
				<?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group required">
              <label class="col-sm-2 control-label"><?php echo $entry_FurnizorCIF; ?></label>
              <div class="col-sm-10">
                  <input type="text" name="agv_saga_FurnizorCIF" value="<?php echo $agv_saga_FurnizorCIF; ?>" placeholder="<?php echo $entry_FurnizorCIF; ?>" id="input-FurnizorCIF" class="form-control"/>
                  <?php if ($error_FurnizorCIF) { ?>
                  <div class="text-danger"><?php echo $error_FurnizorCIF; ?></div>
                  <?php } ?>
              </div>
          </div>
          <div class="form-group required">
              <label class="col-sm-2 control-label"><?php echo $entry_Gestiune; ?></label>
              <div class="col-sm-10">
                  <input type="text" name="agv_saga_Gestiune" value="<?php echo $agv_saga_Gestiune; ?>" placeholder="<?php echo $entry_Gestiune; ?>" id="input-Gestiune" class="form-control"/>
                  <?php if ($error_Gestiune) { ?>
                  <div class="text-danger"><?php echo $error_Gestiune; ?></div>
                  <?php } ?>
              </div>
          </div>
          <div class="form-group required">
              <label class="col-sm-2 control-label"><?php echo $entry_FurnizorFacturaSerie; ?></label>
              <div class="col-sm-10">
                  <input type="text" name="agv_saga_FurnizorFacturaSerie" value="<?php echo $agv_saga_FurnizorFacturaSerie; ?>" placeholder="<?php echo $entry_FurnizorFacturaSerie; ?>" id="input-FurnizorFacturaSerie" class="form-control"/>
                  <?php if ($error_FurnizorFacturaSerie) { ?>
                  <div class="text-danger"><?php echo $error_FurnizorFacturaSerie; ?></div>
                  <?php } ?>
              </div>
          </div>
          <div class="form-group required">
              <label class="col-sm-2 control-label"><?php echo $entry_shipping_code; ?></label>
              <div class="col-sm-10">
                  <input type="text" name="agv_saga_shipping_code" value="<?php echo $agv_saga_shipping_code; ?>" placeholder="<?php echo $entry_shipping_code; ?>" id="input-shipping_code" class="form-control"/>
                  <?php if ($error_shipping_code) { ?>
                  <div class="text-danger"><?php echo $error_shipping_code; ?></div>
                  <?php } ?>
              </div>
          </div>
          <div class="form-group required">
              <label class="col-sm-2 control-label"><?php echo $entry_coupon_code; ?></label>
              <div class="col-sm-10">
                  <input type="text" name="agv_saga_coupon_code" value="<?php echo $agv_saga_coupon_code; ?>" placeholder="<?php echo $entry_coupon_code; ?>" id="input-coupon_code" class="form-control"/>
                  <?php if ($error_coupon_code) { ?>
                  <div class="text-danger"><?php echo $error_coupon_code; ?></div>
                  <?php } ?>
              </div>
          </div>
		</form>
	  </div>
	</div>
  </div>
</div>
<?php echo $footer; ?>
