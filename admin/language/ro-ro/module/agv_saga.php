<?php
// Heading
$_['heading_title']    = 'AGV Export Saga';

$_['text_extension']   = 'Module';
$_['text_success']     = 'Succes: Ai modificat modul AGV Saga!';
$_['text_edit']        = 'Editează modulul AGV Saga';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Eroare: Nu ai permisiunile necesare pentru a modifica această pagină. Daca ești administrator mergi in admin la grupuri de utilizatori și actualizează permisiunile!';

//
$_['text_no_orders']   = 'Vă rugam selectati cel puțin o comandă pentru export.';

$_['text_enabled']     = 'Activat';
$_['text_disabled']    = 'Dezactivat';

$_['button_save']      = 'Salvează';
$_['button_cancel']    = 'Anulează';

//entry
$_['entry_FurnizorNume']                   = 'Nume firmă';
$_['entry_FurnizorCIF']                    = 'CIF';
$_['entry_FurnizorNrRegCom']               = 'Nr. Reg. Com.';
$_['entry_FurnizorCapital']                = 'Capital social';
$_['entry_FurnizorAdresa']                 = 'Adresa';
$_['entry_FurnizorBanca']                  = 'Banca';
$_['entry_FurnizorIBAN']                   = 'IBAN';
$_['entry_FurnizorInformatiiSuplimentare'] = 'Informatii suplimentare';
$_['entry_Gestiune']                       = 'Gestiune implicită';
$_['entry_FurnizorFacturaSerie']           = 'Serie factură';
$_['entry_shipping_title']                 = 'Descriere metodă livrare';
$_['entry_shipping_code']                  = 'Cod articol metodă livrare';
$_['entry_coupon_title']                   = 'Descriere cupon reducere';
$_['entry_coupon_code']                    = 'Cod articol cupon reducere';

$_['error_FurnizorNume']                   = 'Nume firmă este câmp obligatoriu!';
$_['error_FurnizorCIF']                    = 'CIF este câmp obligatoriu!';
$_['error_FurnizorNrRegCom']               = 'Nr. Reg. Com. este câmp obligatoriu!';
$_['error_FurnizorCapital']                = 'Capital social este câmp obligatoriu!';
$_['error_FurnizorAdresa']                 = 'Adresa este câmp obligatoriu!';
$_['error_FurnizorBanca']                  = 'Banca este câmp obligatoriu!';
$_['error_FurnizorIBAN']                   = 'IBAN este câmp obligatoriu!';
$_['error_FurnizorInformatiiSuplimentare'] = 'Informatii suplimentare este câmp obligatoriu!';
$_['error_Gestiune']                       = 'Nume gestiune este câmp obligatoriu!';
$_['error_FurnizorFacturaSerie']           = 'Serie factura este câmp obligatoriu!';
$_['error_shipping_title']                 = 'Descriere metoda livrare este câmp obligatoriu!';
$_['error_shipping_code']                  = 'Cod articol metoda livrare este câmp obligatoriu!';
$_['error_coupon_title']                   = 'Descriere cupon reducere este câmp obligatoriu!';
$_['error_coupon_code']                    = 'Cod articol cupon reducere este câmp obligatoriu!';
