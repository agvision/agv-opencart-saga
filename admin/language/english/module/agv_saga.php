<?php
// Heading
$_['heading_title']    = 'AGV Export Saga';

$_['text_extension']   = 'Modules';
$_['text_success']     = 'Success: You have updated AGV Saga!';
$_['text_edit']        = 'Edit AGV Saga';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify AGV Saga!';

//
$_['text_no_orders']   = 'Please select at least one order for export..';

$_['text_enabled']     = 'Activated';
$_['text_disabled']    = 'Deactivated';

$_['button_save']      = 'Save';
$_['button_cancel']    = 'Cancel';

//entry
$_['entry_FurnizorNume']                   = 'Company name';
$_['entry_FurnizorCIF']                    = 'CIF';
$_['entry_FurnizorNrRegCom']               = 'Reg. Com. No.';
$_['entry_FurnizorCapital']                = 'Social capital';
$_['entry_FurnizorAdresa']                 = 'Address';
$_['entry_FurnizorBanca']                  = 'Bank';
$_['entry_FurnizorIBAN']                   = 'IBAN';
$_['entry_FurnizorInformatiiSuplimentare'] = 'Extra information';
$_['entry_Gestiune']                       = 'Default gestion';
$_['entry_FurnizorFacturaSerie']           = 'Invoice series';
$_['entry_shipping_title']                 = 'Shipping method';
$_['entry_shipping_code']                  = 'Shipping method code';
$_['entry_coupon_title']                   = 'Discount coupon';
$_['entry_coupon_code']                    = 'Discount coupon code';

$_['error_FurnizorNume']                   = 'Nume firmă is a mandatory field';
$_['error_FurnizorCIF']                    = 'CIF is a mandatory field!!';
$_['error_FurnizorNrRegCom']               = 'Reg. Com. No. is a mandatory field!!';
$_['error_FurnizorCapital']                = 'Social capital is a mandatory field!!';
$_['error_FurnizorAdresa']                 = 'Address is a mandatory field!!';
$_['error_FurnizorBanca']                  = 'Bank is a mandatory field!!';
$_['error_FurnizorIBAN']                   = 'IBAN is a mandatory field!!';
$_['error_FurnizorInformatiiSuplimentare'] = 'Extra information is a mandatory field!!';
$_['error_Gestiune']                       = 'Gestion name is a mandatory field!!';
$_['error_FurnizorFacturaSerie']           = 'Invoice series is a mandatory field!!';
$_['error_shipping_title']                 = 'Shipping method is a mandatory field!!';
$_['error_shipping_code']                  = 'Shipping method code is a mandatory field!!';
$_['error_coupon_title']                   = 'Discount coupon is a mandatory field!!';
$_['error_coupon_code']                    = 'Discount coupon code is a mandatory field!!';
