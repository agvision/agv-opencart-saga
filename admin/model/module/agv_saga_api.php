<?php

class ModelModuleAgvSagaApi
{
    private $apiUrl = 'https://hotbread.agvision.ro/api/v1/agv-saga/';

    public function __construct()
    {
    }

    /**
     * Make a call to the API.
     *
     * @param string $method Name of endpoint to call.
     * @param array $data    Data to send along.
     * @return void
     */
    public function call($method, array $data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->apiUrl . $method);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, false);
        if (function_exists('gzinflate')) {
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        }
        curl_setopt($ch, CURLOPT_REFERER, $_SERVER['HTTP_HOST']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            echo curl_error($ch);
        } else {
            $json     = json_decode($result);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            $return = [
                'message'  => (isset($json->message) ? $json->message : ''),
                'httpCode' => $httpCode
            ];
            
            curl_close($ch);

            return $return;
        }
    }
}
