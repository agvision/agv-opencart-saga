<?php

class ModelModuleAgvSaga extends Model
{
    protected $fields = ['FurnizorCIF', 'FurnizorFacturaSerie', 'Gestiune', 'coupon_code', 'shipping_code'];

    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('module/agv_saga_api');
        $this->load->model('sale/order');
        $this->load->model('catalog/product');
    }

    /**
     * Parse orders given and return XML.
     * @param  array $orders Order ID array sent from controller
     * @return string        XML format of Saga orders
     */
    public function getXML(array $orders)
    {
        $data = [
            'key'    => $this->config->get('agv_saga2_key'),
            'type'   => 'export',
            'orders' => [],
        ];

        foreach ($orders as $order_id) {
            $temp['order']  = $this->model_sale_order->getOrder($order_id);
            $temp['totals'] = $this->model_sale_order->getOrderTotals($order_id);
            $products       = $this->model_sale_order->getOrderProducts($order_id);
            $i              = 0;

            // For each order products add product location
            foreach ($products as $product) {
                $productDetails           = $this->model_catalog_product->getProduct($product['product_id']);
                $products[$i]['location'] = $productDetails['location'];

                $i++;
            }
            $temp['products'] = $products;

            // Add relevant custom Saga fields
            foreach ($this->fields as $field) {
                $keyPost = 'agv_saga_'.$field;

                $temp['client'][$keyPost] = $this->config->get($keyPost);
            }

            $data['orders'][] = $temp;
        }

        $export = $this->model_module_agv_saga_api->call('call', $data);

        return $export['message'];
    }
}
