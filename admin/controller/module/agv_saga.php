<?php

class ControllerModuleAgvSaga extends Controller
{
    private $error = array();
    protected $fields = ['FurnizorCIF', 'FurnizorFacturaSerie', 'Gestiune', 'shipping_code', 'coupon_code'];
    protected $version = '1.0.3';

    public function __construct($registry)
    {
        parent::__construct($registry);

        $this->load->model('user/user_group');
        $this->load->model('setting/setting');
        $this->load->model('module/agv_saga');
        $this->load->model('module/agv_saga_api');
    }

    public function index()
    {
        $this->load->language('module/agv_saga');

        $this->document->setTitle($this->language->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('agv_saga', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('module/agv_saga', 'token=' . $this->session->data['token'] . '&type=module', true));
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('module/agv_saga', 'token=' . $this->session->data['token'], true)
        );

        $data['action'] = $this->url->link('module/agv_saga', 'token=' . $this->session->data['token'], true);

        $data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

        $fields = array_merge($this->fields, ['module_status']);

        foreach ($fields as $field) {
            $keyPost  = 'agv_saga_'.$field;
            $keyEntry = 'entry_'.$field;
            $keyError = 'error_'.$field;

            if (isset($this->request->post[$keyPost])) {
                $data[$keyPost] = $this->request->post[$keyPost];
            } else {
                $data[$keyPost] = $this->config->get($keyPost);
            }

            // Set Labels
            if ($field !== 'module_status') {
                $data[$keyEntry] = $this->language->get($keyEntry);

            // Set Errors
                if (isset($this->error[$field])) {
                    $data[$keyError] = $this->error[$field];
                } else {
                    $data[$keyError] = '';
                }
            }
        }

        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['entry_status']  = $this->language->get('entry_status');
        $data['text_enabled']  = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');

        $data['button_save']   = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        $data['header']        = $this->load->controller('common/header');
        $data['column_left']   = $this->load->controller('common/column_left');
        $data['footer']        = $this->load->controller('common/footer');

        // Check if Shop is registered
        $this->register();

        $this->response->setOutput($this->load->view('module/agv_saga.tpl', $data));
    }

    /**
     * Validate field save actions.
     *
     * @return void
     */
    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'module/agv_saga')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        
        foreach ($this->fields as $field) {
            $keyPost  = 'agv_saga_'.$field;
            $keyError = 'error_'.$field;

            //set errors
            if (!$this->request->post[$keyPost]) {
                $this->error[$field] = $this->language->get($keyError);
            }
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Install module by adding permissions.
     *
     * @return void
     */
    protected function install()
    {
        $data = array();
        $data['config_name']      = $this->config->get('config_name');
        $data['config_email']     = $this->config->get('config_email');
        $data['config_telephone'] = $this->config->get('config_telephone');

        // Register shop
        $this->register();

        // Add module permissions
        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'module/agv_saga');
        $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'module/agv_saga');
    }

    /**
     * Register new client to API.
     *
     * @return void
     */
    protected function register()
    {
        if ($this->config->get('agv_saga2_registered') != 1) {
            // Register Shop
            $data = array();
            $data['platform']         = 0;
            $data['key']              = $this->generateKey(10);
            $data['version']          = VERSION;
            $data['module_version']   = $this->version;
            $data['config_name']      = $this->config->get('config_name');
            $data['config_email']     = $this->config->get('config_email');
            $data['config_telephone'] = $this->config->get('config_telephone');

            $register = $this->model_module_agv_saga_api->call('register', $data);

            if ($register['httpCode'] === 201) {
                // If response code created then create settings
                $this->model_setting_setting->editSetting('agv_saga2', [
                    'agv_saga2_registered' => 1,
                    'agv_saga2_key'        => $data['key']
                ]);
            } elseif ($register['httpCode'] === 409) {
                // If response code conflict then edit settings
                $this->model_setting_setting->editSetting('agv_saga2', [
                    'agv_saga2_registered' => 1,
                    'agv_saga2_key'        => $this->config->get('agv_saga2_key')
                ]);
            }
        }
    }

    /**
     * Return output of exported orders in XML format.
     * @return string Orders in XML format
     */
    public function exportXML()
    {
        if (isset($this->request->post['selected'])) {
            $xml = $this->model_module_agv_saga->getXML($this->request->post['selected']);
        } else {
            $xml = $this->language->get('text_no_orders');
        }

        $series = $this->config->get('agv_saga_FurnizorFacturaSerie');
        $cui    = $this->config->get('agv_saga_FurnizorCIF');
        $date   = date('d.m.Y', time());

        $this->response->addHeader('Content-Type: application/xml');
        $this->response->addHeader("Content-Disposition: attachment; filename=F_{$cui}_{$series}_00001_{$date}.xml");
        $this->response->setOutput($xml);
    }

    /**
     * Generate random key for Shop.
     *
     * @param int $length
     * @return void
     */
    public function generateKey($length)
    {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}
